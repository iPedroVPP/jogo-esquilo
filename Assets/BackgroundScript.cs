﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundScript : MonoBehaviour
{
    private float screenWidth;
    void Start()
    {
        Rigidbody2D body = GetComponent<Rigidbody2D>();
        SpriteRenderer renderer = GetComponent<SpriteRenderer>();

        float imageWidth = renderer.sprite.bounds.size.x;
        float imageHeight = renderer.sprite.bounds.size.y;

        float screenHeight = Camera.main.orthographicSize * 2;
        screenWidth = screenHeight / Screen.height * Screen.width;
        
        Vector2 newScale = this.transform.localScale;

        newScale.x = screenWidth / imageWidth;
        newScale.y = screenHeight / imageHeight;

        this.transform.localScale = newScale;


    }

    // Update is called once per frame
    void Update()
    {

    }
}
