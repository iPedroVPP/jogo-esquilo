﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EsquiloScript : MonoBehaviour
{
    private Rigidbody2D body;
    private SpriteRenderer render;

    private Animator animator;
    void Start()
    {
        this.render = GetComponent<SpriteRenderer>();
        this.body = GetComponent<Rigidbody2D>();
        this.animator = GetComponent<Animator>();

        this.animator.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {

        float x = this.transform.position.x;
        float y = this.transform.position.y;

        float inputKeyHorizontal = Input.GetAxis("Horizontal");

        if (inputKeyHorizontal > 0)
        {
            this.body.AddForce(new Vector2(2,0));
            this.render.flipX = false;
            this.animator.enabled = true;
        }

        else if (inputKeyHorizontal < 0)
        {
            this.body.AddForce(new Vector2(-2,0));
            this.render.flipX = true;
            this.animator.enabled = true;
        }
        else 
        {
            this.body.MovePosition(new Vector2(x , y));
            this.animator.enabled = false;
        }

    }
}
